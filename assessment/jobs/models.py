from django.db import models


class Example(models.Model):
    example = models.TextField()

    def __str__(self):
        return self.example

class Tag(models.Model):
    tag_name = models.CharField(max_length=200)
    tag_desc = models.TextField()
    img_src = models.URLField(max_length=255)

class JobPosting(models.Model):
    job_title = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200)
    company_location = models.CharField(max_length=200)
    min_salary = models.IntegerField()
    max_salary = models.IntegerField()
    apply_link = models.URLField(max_length = 255)
    tags = models.ManyToManyField(Tag)