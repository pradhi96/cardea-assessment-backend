import graphene

from graphene_django import DjangoObjectType
from assessment.jobs.models import JobPosting, Tag

class TagType(DjangoObjectType):
    class Meta:
        model = Tag
        fields = ("tag_name", "tag_desc", "img_src")

class JobPostingType(DjangoObjectType):
    class Meta:
        model = JobPosting
        fields = ("job_title", "company_name", "company_location", "min_salary", "max_salary", "apply_link")

class Query(graphene.ObjectType):
    hello = graphene.String(default_value="Hi Test!")
    all_tags = graphene.List(TagType)
    job_posting_by_tag = graphene.List(JobPostingType, name=graphene.String(required=True))
    job_posting_by_title = graphene.Field(JobPostingType, title=graphene.String(required=True))
    def resolve_all_tags(root, info):
        return Tag.objects.all()
    def resolve_job_posting_by_tag(root, info, name):
        relevant_job_postings = []
        try:
            all_job_postings = JobPosting.objects.all()
            for posting in all_job_postings:
                all_tags = posting.tags.all()
                for tag in all_tags:
                    if tag.tag_name == name:
                        relevant_job_postings.append(posting)
                        break
            
            return relevant_job_postings
        except JobPosting.DoesNotExist:
            return None
    
    def resolve_job_posting_by_title(root, info, title):
        return JobPosting.objects.get(job_title = title)


schema = graphene.Schema(query=Query)